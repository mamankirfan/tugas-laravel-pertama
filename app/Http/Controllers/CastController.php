<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Cast = Cast::all();

        return view('Cast.index', compact('Cast'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request$request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|max:255'
         ],
         [
            'nama.required' => 'Nama Harus diisi',
            'umur.required'  => 'Umur Harus diisi',
            'bio.required'  => 'Bio tidak boleh kosong',
            'bio.max'  => 'Bio tidak boleh lebih dari 255',

         ]
        );


        $Cast = new Cast;
 
        $Cast->nama = $request->nama;
        $Cast->umur = $request->umur;
        $Cast->bio = $request->bio;
 
        $Cast->save();

        return redirect('/Cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Cast = Cast::find($id)->first();

        return view('Cast.show', compact ('Cast'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Cast = Cast::find($id);

        return view('Cast.edit', compact ('Cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required|max:255'
            ],
            [
                'nama.required' => 'Nama Harus diisi',
                'umur.required'  => 'Umur Harus diisi',
                'bio.required'  => 'Bio tidak boleh kosong',
                'bio.max'  => 'Bio tidak boleh lebih dari 255',

            ]
        );
        $Cast = Cast::find($id);
 
        $Cast->nama = $request['nama'];
        $Cast->umur = $request['umur'];
        $Cast->bio = $request['bio'];
        
        $Cast->save();

        return redirect ('/Cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Cast = Cast::find($id);
 
        $Cast->delete();
        return redirect('/Cast');
    }
}
