<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('page.register');
    }

    public function kirim(Request $request){
        // dd($request->all());
        $first_name = $request['first_name'];    
        $last_name = $request['last_name'];   
        
        return view('page.welcome', compact("first_name", "last_name"));

    }

}
