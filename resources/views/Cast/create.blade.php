@extends('layout.master')
@section('title')
    halaman Tambah Cast
@endsection
@section('content')

<form method="POST" action="/Cast">
    @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger" role="alert">{{ $message }}</div>    
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger" role="alert">{{ $message }}</div>    
    @enderror
    <div class="form-group">
      <label>bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>    
    </div>
    @error('bio')
    <div class="alert alert-danger" role="alert">{{ $message }}</div>    
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection