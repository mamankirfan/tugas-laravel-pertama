@extends('layout.master')
@section('title')
    halaman Edit Cast
@endsection
@section('content')

<form method="POST" action="/Cast/{{$Cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="nama" value="{{$Cast->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger" role="alert">{{ $message }}</div>    
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur" value="{{$Cast->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger" role="alert">{{ $message }}</div>    
    @enderror
    <div class="form-group">
      <label>bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control">{{$Cast->bio}}</textarea>    
    </div>
    @error('bio')
    <div class="alert alert-danger" role="alert">{{ $message }}</div>    
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection