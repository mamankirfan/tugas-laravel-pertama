<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'indexController@index');
Route::get('/biodata', 'AuthController@bio');
Route::post('/kirim', "AuthController@kirim");

Route::get('/data-table', 'indexController@table');


//CRUD Cast
//create 
Route::get('/Cast/create', 'CastController@create');//kearah form tambah data
Route::post('/Cast', 'CastController@store');//menyimpan data form ke DB table Cast

//READ Cast
Route::get('/Cast', 'CastController@index'); //ambil data ke database dan ditampilkan di blade
Route::get('/Cast/{Cast_id}', 'CastController@show'); //route detail Cast

//UPDATE Cast
Route::get('/Cast/{Cast_id}/edit', 'CastController@edit')->name('user'); //Route Untuk Mengarah ke Halaman Edit
Route::put('/Cast/{Cast_id}', 'CastController@update')->name('user'); //Route Untuk Mengarah ke Halaman Edit


//DELETE Cast
Route::delete('/Cast/{Cast_id}', 'CastController@destroy'); //Route Delete Data berdasarkan id
